module gorillaSocketServer

go 1.21.3

require (
	github.com/gorilla/websocket v1.5.1
	gopkg.in/rethinkdb/rethinkdb-go.v6 v6.2.2
)

require (
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/sirupsen/logrus v1.0.6 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/term v0.13.0 // indirect
	gopkg.in/cenkalti/backoff.v2 v2.2.1 // indirect
)
